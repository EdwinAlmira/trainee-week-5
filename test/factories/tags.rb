# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :tag do
    tag_name { Faker::Food.ethnic_category }
  end
end
