# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :product do
    name { Faker::Food.dish }
    price { Faker::Commerce.price }
    stock { rand(20..100) }
    description { Faker::Food.description }
    popularity { 0 }
    after(:build) do |product|
      product.picture.attach(
        io: File.open(Rails.root.join('test', 'fixture_files', 'test.jpg')),
        filename: 'test.jpg',
        content_type: 'image/jpeg'
      )
    end
  end
end
