# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :price_change do
    product
    old_price { Faker::Commerce.price }
  end
end
