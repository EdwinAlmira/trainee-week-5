# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user_review do
    association :reviewer, factory: :user
    association :reviewed, factory: :user
    status { 0 }
    review { Faker::Lorem.sentence(word_count: 5, supplemental: true) }
  end
end
