# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :cart do
    user
    status { 0 }
    total { Faker::Commerce.price }

    factory :cart_wo_user do
      status { 0 }
      total { Faker::Commerce.price }
    end
  end
end
