# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :comment do
    association :commented, factory: :purchase
    user
    description { Faker::Food.description }

    factory :comment_wo_description do
      association :commented, factory: :product
      user
      description { Faker::Food.description }
    end
  end
end
