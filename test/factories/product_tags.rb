# frozen_string_literal: true

FactoryBot.define do
  factory :product_tag do
    product
    tag
  end
end
