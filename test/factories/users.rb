# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user do
    user_type
    email { Faker::Internet.email }
    password { Faker::Internet.password }

    factory :customer do
      user_type_id { 2 }
      email { Faker::Internet.email }
      password { Faker::Internet.password }
    end
    factory :admin do
      user_type_id { 1 }
      email { Faker::Internet.email }
      password { Faker::Internet.password }
    end
  end
end
