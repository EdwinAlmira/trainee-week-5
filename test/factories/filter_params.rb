# frozen_string_literal: true

FactoryBot.define do
  factory :filter_params, class: Products::FilterParams do
    order { 'asc' }
    filter_tag { '' }
    attribute { 'name' }
    page { 1 }
    size { 2 }
  end

  factory :wrong_attribute, class: Products::FilterParams do
    order { 'asc' }
    filter_tag { '' }
    attribute { 'wrong' }
    page { 1 }
    size { 2 }
  end

  factory :wrong_order, class: Products::FilterParams do
    order { 'wrong' }
    filter_tag { '' }
    attribute { 'name' }
    page { 1 }
    size { 2 }
  end
end
