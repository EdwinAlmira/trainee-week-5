# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :purchase do
    product
    cart
    quantity { rand(1..10) }
    status { rand(0..1) }

    factory :active_purchase do
      product
      cart
      quantity { rand(1..10) }
      status { 0 }
    end
  end
end
