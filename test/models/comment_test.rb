# frozen_string_literal: true

require './test/test_helper'

class CommentTest < ActiveSupport::TestCase
  def setup
    @comment = FactoryBot.build(:comment)
  end

  test 'valid comment' do
    assert @comment.valid?
  end

  test 'comment without description' do
    assert_not_nil FactoryBot.build(:comment_wo_description).errors[:description]
  end

  test 'comment without user' do
    @comment.user_id = nil
    assert_not_nil @comment.errors[:user_id]
  end

  test 'comment without commented' do
    @comment.commented_id = nil
    assert_not_nil @comment.errors[:commented_id]
  end

  test '#purchase_already_commented' do
    assert Comment.purchase_already_commented(@comment)
  end
end
