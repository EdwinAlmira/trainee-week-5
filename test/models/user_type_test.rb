# frozen_string_literal: true

require './test/test_helper'

class UserTypeTest < ActiveSupport::TestCase
  def setup
    @user_type = FactoryBot.build(:user_type)
  end

  test 'valid user type' do
    assert @user_type.valid?
  end
end
