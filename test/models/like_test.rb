# frozen_string_literal: true

require './test/test_helper'

class LikeTest < ActiveSupport::TestCase
  def setup
    @like = FactoryBot.build(:like)
  end

  test 'valid like' do
    assert @like.valid?
  end

  test 'like without user' do
    @like.user_id = nil
    assert_not_nil @like.errors[:user_id]
  end

  test 'like without product' do
    @like.product_id = nil
    assert_not_nil @like.errors[:product_id]
  end

  test '#already_liked' do
    assert Like.already_liked(@like.user_id, @like.product_id)
  end
end
