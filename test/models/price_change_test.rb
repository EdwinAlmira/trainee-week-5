# frozen_string_literal: true

require './test/test_helper'

class PriceChangeTest < ActiveSupport::TestCase
  def setup
    @price_change = FactoryBot.build(:price_change)
  end

  test 'valid price change' do
    assert @price_change.valid?
  end

  test 'price change without old price' do
    @price_change.old_price = nil
    assert_not_nil @price_change.errors[:old_price]
  end

  test 'price change without product' do
    @price_change.product_id = nil
    assert_not_nil @price_change.errors[:product_id]
  end

  test '#did_the_price_change' do
    assert PriceChange.did_the_price_change(@price_change)
  end
end
