# frozen_string_literal: true

require './test/test_helper'

class ProductTagTest < ActiveSupport::TestCase
  def setup
    @product_tag = FactoryBot.build(:product_tag)
  end

  test 'valid product tag' do
    assert @product_tag.valid?
  end

  test 'product tag without product' do
    @product_tag.product_id = nil
    assert_not_nil @product_tag.errors[:product_id]
  end

  test 'product tag without tag' do
    @product_tag.tag_id = nil
    assert_not_nil @product_tag.errors[:tag_id]
  end
end
