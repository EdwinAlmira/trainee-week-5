# frozen_string_literal: true

require './test/test_helper'

class CartTest < ActiveSupport::TestCase
  def setup
    @cart = FactoryBot.build(:cart)
  end

  test 'valid cart' do
    assert @cart.valid?
  end

  test 'cart without user' do
    @cart = FactoryBot.build(:cart_wo_user)
    assert_not_nil @cart.errors[:user_id]
  end

  test '#has_cart_active' do
    assert Cart.has_cart_active(@cart)
  end
end
