# frozen_string_literal: true

require './test/test_helper'

class PurchaseTest < ActiveSupport::TestCase
  def setup
    @purchase = FactoryBot.build(:purchase)
  end

  test 'valid purchase' do
    assert @purchase.valid?
  end
end
