# frozen_string_literal: true

require './test/test_helper'

class ProductTest < ActiveSupport::TestCase
  def setup
    @product = FactoryBot.build(:product)
  end

  test 'valid product' do
    assert @product.valid?
  end

  # test 'invalid without name' do
  #   @product.name = nil
  #   refute @product.valid?, 'saved product without a name'
  #   assert_not_nil @product.errors[:name], 'no validation error for name present'
  # end

  # test 'invalid without price' do
  #   @product.price = nil
  #   refute @product.valid?, 'saved product without a price'
  #   assert_not_nil @product.errors[:price], 'no validation error for price present'
  # end

  # test 'invalid without stock' do
  #   @product.stock = nil
  #   refute @product.valid?, 'saved product without a stock'
  #   assert_not_nil @product.errors[:stock], 'no validation error for stock present'
  # end

  # test 'invalid stock quantity' do
  #   @product.stock = 0
  #   refute @product.valid?, 'Saved a product with wrong stock value'
  #   assert_not_nil @product.errors[:stock], 'no validation error for stock value'
  # end

  # test 'invalid price' do
  #   @product.price = 0
  #   refute @product.valid?, 'saved product with wrong price'
  #   assert_not_nil @product.errors[:price], 'no validation error for price value'
  # end

  # test '#like' do
  #   assert_equal 0, @product.likes.size
  # end
end
