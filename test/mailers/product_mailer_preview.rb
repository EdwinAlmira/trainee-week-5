# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/product_mailer
class ProductMailerPreview < ActionMailer::Preview
  def run_out_of_stock
    ProductMailer.with(user: User.first, product: Product.first).run_out_of_stock
  end
end
