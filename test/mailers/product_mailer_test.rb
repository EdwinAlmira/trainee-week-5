# frozen_string_literal: true

require './test/test_helper'

class ProductMailerTest < ActionMailer::TestCase
  def setup
    @user = FactoryBot.build(:user)
    @product = FactoryBot.build(:product)
    @product.save
  end

  test 'the emails is sended' do
    assert_emails 0
    ProductMailer.with(user: @user, product: @product).run_out_of_stock.deliver_now
  end

  test 'no emails are enqueued' do
    assert_no_enqueued_emails do
      ProductMailer.with(user: @user, product: @product).run_out_of_stock.deliver_now
    end
  end
end
