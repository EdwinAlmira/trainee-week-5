# frozen_string_literal: true

require './test/test_helper'

class SumLikesTest < ActiveJob::TestCase
  include ActiveJob::TestHelper
  def setup
    @product = FactoryBot.build(:product)
    @product.save
  end

  test 'enqueue the job' do
    assert_enqueued_jobs 1 do
      Likes::SumLikes.perform_later(@product)
    end
  end

  test 'right arguments and queue' do
    assert_enqueued_with(job: Likes::SumLikes, args: [@product], queue: 'default') do
      Likes::SumLikes.perform_later(@product)
    end
  end

  test 'job is performed' do
    assert_performed_jobs 1 do
      Likes::SumLikes.perform_later(@product)
    end
  end
end
