# frozen_string_literal: true

require './test/test_helper'

class SubsLikesTest < ActiveJob::TestCase
  include ActiveJob::TestHelper
  def setup
    @product = FactoryBot.build(:product)
    @product.save
  end

  test 'enqueue the job' do
    assert_enqueued_jobs 1 do
      Likes::SubsLikes.perform_later(@product)
    end
  end

  test 'right arguments and queue' do
    assert_enqueued_with(job: Likes::SubsLikes, args: [@product], queue: 'default') do
      Likes::SubsLikes.perform_later(@product)
    end
  end

  test 'job is performed' do
    assert_performed_jobs 1 do
      Likes::SubsLikes.perform_later(@product)
    end
  end
end
