# frozen_string_literal: true

require './test/test_helper'

class RunOutOfStockTest < ActiveJob::TestCase
  include ActiveJob::TestHelper
  def setup
    @cart = FactoryBot.create(:cart)
  end

  test 'enqueue the job' do
    assert_enqueued_jobs 1 do
      Products::RunOutOfStockJob.perform_later(@cart)
    end
  end

  test 'right arguments and queue' do
    assert_enqueued_with(job: Products::RunOutOfStockJob, args: [@cart], queue: 'default') do
      Products::RunOutOfStockJob.perform_later(@cart)
    end
  end

  test 'job is performed' do
    assert_performed_jobs 1 do
      Products::RunOutOfStockJob.perform_later(@cart)
    end
  end
end
