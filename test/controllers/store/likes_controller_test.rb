# frozen_string_literal: true

require 'test_helper'

module Store
  class LikesControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    def setup
      @user = FactoryBot.create(:customer)
      @product = FactoryBot.create(:product)
      @like = FactoryBot.create(:like)
    end

    test 'should create a purchase' do
      sign_in @user
      post store_likes_path(user_id: @user.id, product_id: @product.id)
      assert_redirected_to inventory_products_path
      assert_equal 'liked', flash[:notice]
    end

    test 'should delete a purchase' do
      sign_in @user
      delete store_like_path(id: @like.id, user_id: @like.user_id, product_id: @like.product_id)
      assert_redirected_to inventory_products_path
      assert_equal 'disliked', flash[:notice]
    end
  end
end
