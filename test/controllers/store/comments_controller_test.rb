# frozen_string_literal: true

require 'test_helper'

module Store
  class CommentsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    def setup
      @user = FactoryBot.create(:customer)
      @product = FactoryBot.create(:product)
      @purchase = FactoryBot.create(:purchase)
      @commented = FactoryBot.build(:comment)
    end

    test 'should make purchase a comment' do
      sign_in @user
      post store_comments_path, params: { type: 'purchase', id: @purchase.id,
                                          user_id: @user.id, description: @commented.description }
      assert_redirected_to inventory_products_path
      assert_equal 'Comment saved', flash[:notice]
    end

    test 'should make product a comment' do
      sign_in @user
      post store_comments_path, params: { type: 'product', id: @product.id,
                                          user_id: @user.id, description: @commented.description }
      assert_redirected_to inventory_products_path
      assert_equal 'Comment saved', flash[:notice]
    end
  end
end
