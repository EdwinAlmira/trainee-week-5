# frozen_string_literal: true

require 'test_helper'

module Inventory
  class ProductTagsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    def setup
      @user = FactoryBot.build(:admin)
      @product = FactoryBot.create(:product)
      @tag = FactoryBot.create(:tag)
      @already_tagged = FactoryBot.create(:product_tag)
    end

    test 'should assign a tag to a product' do
      sign_in @user
      post inventory_product_tags_path, params: { tag_id: @tag.id, product_id: @product.id }
      assert_redirected_to inventory_products_path
      assert_equal 'Tag added to the product', flash[:notice]
    end

    test 'product already tagged' do
      sign_in @user
      post inventory_product_tags_path,
           params: { tag_id: @already_tagged.tag_id, product_id: @already_tagged.product_id }
      assert_response 400
      assert_equal 'product already tagged', json_response['message']['tag_id'][0]
    end

    def json_response
      ActiveSupport::JSON.decode @response.body
    end
  end
end
