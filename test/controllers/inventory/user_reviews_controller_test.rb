# frozen_string_literal: true

require 'test_helper'

module Inventory
  class UserReviewsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    def setup
      @admin = FactoryBot.create(:admin)
      @customer = FactoryBot.create(:customer)
      @reviewed = FactoryBot.create(:customer)
      @product = FactoryBot.create(:product)
      @user_review = FactoryBot.create(:user_review)
    end

    test 'should get index' do
      sign_in @customer
      get inventory_products_path
      assert_response :success
    end

    test 'should approve a user review' do
      sign_in @admin
      put inventory_user_review_path(id: @user_review.id)
      assert_redirected_to inventory_user_reviews_path
      assert_equal 'Review approved', flash[:notice]
    end

    test 'should create a user review' do
      sign_in @customer
      post inventory_user_reviews_path,
           params: { reviewed_id: @reviewed.id, reviewer_id: @customer.id, review: @user_review.review }
      assert_redirected_to inventory_products_path
      assert_equal 'Review sended. Pending of admin aprovation.', flash[:notice]
    end

    test 'should delete a user review' do
      sign_in @admin
      delete inventory_user_review_path(id: @user_review.id)
      assert_redirected_to inventory_user_reviews_path
      assert_equal 'Review deleted', flash[:notice]
    end
  end
end
