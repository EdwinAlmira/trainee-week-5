# frozen_string_literal: true

require './test/test_helper'

module Inventory
  class UsersControllerTest < ActionDispatch::IntegrationTest
    def setup
      @user = FactoryBot.create(:user)
    end

    test 'should get a user' do
      get inventory_user_path(@user)
      assert_response 206
    end
  end
end
