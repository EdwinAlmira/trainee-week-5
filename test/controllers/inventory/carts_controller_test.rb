# frozen_string_literal: true

require 'test_helper'

module Store
  class CartsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    def setup
      @user = FactoryBot.build(:customer)
      @cart = FactoryBot.create(:cart)
    end

    test 'should redirect for not sign in' do
      get inventory_cart_path(id: @cart.id)
      assert_response 302
    end

    test 'should update cart' do
      sign_in @user
      put inventory_cart_path(id: @cart.id, total: 20.50)
      assert_redirected_to inventory_products_path
      assert_equal 'You buy', flash[:notice]
    end
  end
end
