# frozen_string_literal: true

require './test/test_helper'

module Inventory
  class PurchasesControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    def setup
      @user = FactoryBot.build(:user)
      @product = FactoryBot.create(:product)
      @purchase = FactoryBot.create(:active_purchase)
    end

    test 'should get index' do
      sign_in @user
      get inventory_purchases_path
      assert_response :success
    end

    test 'should get a purchase' do
      sign_in @user
      get inventory_product_path(@purchase)
      assert_response :success
    end

    test 'should create a purchase' do
      sign_in @user
      post inventory_purchases_path(user_id: @user.id, quantity: @purchase.quantity, product_id: @purchase.product_id)
      assert_response :bad_request
    end

    test 'should delete a purchase' do
      sign_in @user
      delete inventory_purchase_path(@purchase)
      assert_redirected_to inventory_products_path
      assert_equal 'Deleted purchase', flash[:notice]
    end
  end
end
