# frozen_string_literal: true

require './test/test_helper'

module Inventory
  class ProductControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    def setup
      @user = FactoryBot.build(:admin)
      @product = FactoryBot.create(:product)
    end

    test 'should get index' do
      get inventory_products_path
      assert_response :success
    end

    test 'should get a product' do
      get inventory_product_path(@product)
      assert_response :success
    end

    test 'should update a product' do
      sign_in @user
      put inventory_product_path(@product), params: { price: 1, stock: 1 }
      assert_redirected_to inventory_product_path
      assert_equal 'Product updated', flash[:notice]
    end

    test 'should create a product' do
      sign_in @user
      post inventory_products_path, params: { product: FactoryBot.build(:product).attributes }
      assert_response 400
    end

    test 'should delete a product' do
      sign_in @user
      delete inventory_product_path(@product)
      assert_redirected_to inventory_products_path
      assert_equal 'Product deleted', flash[:notice]
    end
  end
end
