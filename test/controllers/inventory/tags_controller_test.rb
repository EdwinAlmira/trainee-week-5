# frozen_string_literal: true

require './test/test_helper'

module Inventory
  class TagsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    def setup
      @user = FactoryBot.build(:admin)
      @tag = FactoryBot.build(:tag)
      @tag.save
      @unsaved = FactoryBot.build(:tag)
    end

    test 'authenticated user should get index' do
      sign_in @user
      get inventory_tags_path
      assert_response :success
    end

    test "authenticated user should'nt get index" do
      get inventory_tags_path
      assert_response :redirect
    end

    test 'should create a tag' do
      sign_in @user
      post inventory_tags_path, params: { tag: @unsaved.attributes }
      assert_redirected_to inventory_tags_path
      assert_equal 'Tag added', flash[:notice]
    end

    test 'should delete a tag' do
      sign_in @user
      delete inventory_tag_path(@tag)
      assert_redirected_to inventory_tags_path
      assert_equal 'Tag deleted', flash[:notice]
    end
  end
end
