# frozen_string_literal: true

require 'test_helper'

module Api
  class LikesControllerTest < ActionDispatch::IntegrationTest
    def setup
      @user = FactoryBot.create(:user)
      @product = FactoryBot.create(:product)
      @token = JsonWebToken.encode(user_id: @user.id)
      @wrong_product = FactoryBot.build(:product)
    end

    test 'should make a like' do
      post api_v1_customers_likes_path, params: { product_id: @product.id, token: @token },
                                        headers: { Authorization: "Bearer #{@token}" }
      assert_response 200
    end

    test 'should not make a like' do
      post api_v1_customers_likes_path, params: { product_id: @wrong_product.id, token: @token },
                                        headers: { Authorization: "Bearer #{@token}" }
      assert_response 400
    end
  end
end
