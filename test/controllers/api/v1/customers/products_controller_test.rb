# frozen_string_literal: true

require './test/test_helper'

module Api
  module V1
    module Customers
      class ProductsControllerTest < ActionDispatch::IntegrationTest
        def setup
          @params = FactoryBot.build(:filter_params)
          @wrong_attribute = FactoryBot.build(:wrong_attribute)
          @wrong_order = FactoryBot.build(:wrong_order)
        end

        test 'should listing products' do
          @params.validate!
          get api_v1_customers_products_path(order: @params.order, attribute: @params.attribute,
                                             filter_tag: @params.filter_tag, page: @params.page, size: @params.size)
          assert_response :success
        end

        test 'should not listing products. Wrong attribute' do
          get api_v1_customers_products_path(order: @wrong_attribute.order, attribute: @wrong_attribute.attribute,
                                             filter_tag: @wrong_attribute.filter_tag, page: @wrong_attribute.page,
                                             size: @wrong_attribute.size)
          assert_response :bad_request
        end

        test 'should not listing products. Wrong order' do
          get api_v1_customers_products_path(order: @wrong_order.order, attribute: @wrong_order.attribute,
                                             filter_tag: @wrong_order.filter_tag, page: @wrong_order.page,
                                             size: @wrong_order.size)
          assert_response :bad_request
        end
      end
    end
  end
end
