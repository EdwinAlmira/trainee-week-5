# frozen_string_literal: true

require 'test_helper'

module Api
  module V1
    module Customers
      class PurchasesControllerTest < ActionDispatch::IntegrationTest
        def setup
          @user = FactoryBot.create(:user)
          @product = FactoryBot.create(:product)
          @purchase = FactoryBot.build(:active_purchase)
          @token = JsonWebToken.encode(user_id: @user.id)
        end

        test 'should get index' do
          get api_v1_customers_purchases_path,  headers: { Authorization: "Bearer #{@token}" }
          assert_response 200
        end

        test 'should create a purchase' do
          post api_v1_customers_purchases_path, params: { product_id: @product.id,
                                                          quantity: @purchase.quantity, token: @token },
                                                headers: { Authorization: "Bearer #{@token}" }
          assert_response 200
        end
      end
    end
  end
end
