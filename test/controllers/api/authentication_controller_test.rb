# frozen_string_literal: true

require './test/test_helper'

module Api
  class AuthenticationControllerTest < ActionDispatch::IntegrationTest
    def setup
      @user = FactoryBot.build(:admin)
      @valid_user = FactoryBot.create(:user)
    end

    test 'should login' do
      post api_v1_auth_login_path(email: @valid_user.email, password: @valid_user.password)
      assert_response :success
    end

    test 'should not login' do
      post api_v1_auth_login_path(email: @user.email, password: @user.password)
      assert_response :unauthorized
    end
  end
end
