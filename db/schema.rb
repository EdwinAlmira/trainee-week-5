# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_220_103_063_133) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'active_storage_attachments', force: :cascade do |t|
    t.string 'name', null: false
    t.string 'record_type', null: false
    t.bigint 'record_id', null: false
    t.bigint 'blob_id', null: false
    t.datetime 'created_at', null: false
    t.index ['blob_id'], name: 'index_active_storage_attachments_on_blob_id'
    t.index %w[record_type record_id name blob_id], name: 'index_active_storage_attachments_uniqueness',
                                                    unique: true
  end

  create_table 'active_storage_blobs', force: :cascade do |t|
    t.string 'key', null: false
    t.string 'filename', null: false
    t.string 'content_type'
    t.text 'metadata'
    t.string 'service_name', null: false
    t.bigint 'byte_size', null: false
    t.string 'checksum', null: false
    t.datetime 'created_at', null: false
    t.index ['key'], name: 'index_active_storage_blobs_on_key', unique: true
  end

  create_table 'active_storage_variant_records', force: :cascade do |t|
    t.bigint 'blob_id', null: false
    t.string 'variation_digest', null: false
    t.index %w[blob_id variation_digest], name: 'index_active_storage_variant_records_uniqueness', unique: true
  end

  create_table 'carts', force: :cascade do |t|
    t.bigint 'user_id'
    t.integer 'status', default: 0
    t.decimal 'total', default: '0.0'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['user_id'], name: 'index_carts_on_user_id'
  end

  create_table 'comments', force: :cascade do |t|
    t.bigint 'user_id', null: false
    t.string 'commented_type'
    t.bigint 'commented_id'
    t.text 'description'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index %w[commented_type commented_id], name: 'index_comments_on_commented'
    t.index ['user_id'], name: 'index_comments_on_user_id'
  end

  create_table 'likes', force: :cascade do |t|
    t.bigint 'user_id'
    t.bigint 'product_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['product_id'], name: 'index_likes_on_product_id'
    t.index ['user_id'], name: 'index_likes_on_user_id'
  end

  create_table 'price_changes', force: :cascade do |t|
    t.bigint 'product_id', null: false
    t.decimal 'old_price'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['product_id'], name: 'index_price_changes_on_product_id'
  end

  create_table 'product_tags', force: :cascade do |t|
    t.bigint 'tag_id', null: false
    t.bigint 'product_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['product_id'], name: 'index_product_tags_on_product_id'
    t.index ['tag_id'], name: 'index_product_tags_on_tag_id'
  end

  create_table 'products', force: :cascade do |t|
    t.string 'name'
    t.decimal 'price'
    t.integer 'stock'
    t.text 'description'
    t.integer 'popularity', default: 0
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'purchases', force: :cascade do |t|
    t.bigint 'product_id', null: false
    t.bigint 'cart_id', null: false
    t.integer 'quantity'
    t.integer 'status'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['cart_id'], name: 'index_purchases_on_cart_id'
    t.index ['product_id'], name: 'index_purchases_on_product_id'
  end

  create_table 'tags', force: :cascade do |t|
    t.string 'tag_name'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'user_reviews', force: :cascade do |t|
    t.bigint 'reviewed_id'
    t.bigint 'reviewer_id'
    t.text 'review'
    t.integer 'status'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['reviewed_id'], name: 'index_user_reviews_on_reviewed_id'
    t.index ['reviewer_id'], name: 'index_user_reviews_on_reviewer_id'
  end

  create_table 'user_types', force: :cascade do |t|
    t.string 'utype'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'users', force: :cascade do |t|
    t.bigint 'user_type_id', null: false
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['email'], name: 'index_users_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true
    t.index ['user_type_id'], name: 'index_users_on_user_type_id'
  end

  add_foreign_key 'active_storage_attachments', 'active_storage_blobs', column: 'blob_id'
  add_foreign_key 'active_storage_variant_records', 'active_storage_blobs', column: 'blob_id'
  add_foreign_key 'comments', 'users'
  add_foreign_key 'likes', 'products'
  add_foreign_key 'price_changes', 'products'
  add_foreign_key 'product_tags', 'products'
  add_foreign_key 'product_tags', 'tags'
  add_foreign_key 'purchases', 'carts'
  add_foreign_key 'purchases', 'products'
  add_foreign_key 'user_reviews', 'users', column: 'reviewed_id'
  add_foreign_key 'user_reviews', 'users', column: 'reviewer_id'
  add_foreign_key 'users', 'user_types'
end
