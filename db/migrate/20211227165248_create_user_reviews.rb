# frozen_string_literal: true

class CreateUserReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :user_reviews do |t|
      t.references :reviewed, foreign_key: { to_table: :users }
      t.references :reviewer, foreign_key: { to_table: :users }
      t.text :review
      t.integer :status

      t.timestamps
    end
  end
end
