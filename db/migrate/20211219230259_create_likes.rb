# frozen_string_literal: true

class CreateLikes < ActiveRecord::Migration[6.1]
  def change
    create_table :likes do |t|\
      t.belongs_to :user, null: true
      t.belongs_to :product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
