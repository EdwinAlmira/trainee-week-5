# frozen_string_literal: true

class CreateUserTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :user_types do |t|
      t.string :utype

      t.timestamps
    end
  end
end
