# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price
      t.integer :stock
      t.text :description
      t.integer :popularity, default: 0

      t.timestamps
    end
  end
end
