# frozen_string_literal: true

class CreateCarts < ActiveRecord::Migration[6.1]
  def change
    create_table :carts do |t|
      t.belongs_to :user, null: true
      t.integer :status, default: 0
      t.decimal :total, default: 0

      t.timestamps
    end
  end
end
