# frozen_string_literal: true

class CreatePriceChanges < ActiveRecord::Migration[6.1]
  def change
    create_table :price_changes do |t|
      t.belongs_to :product, null: false, foreign_key: true
      t.decimal :old_price

      t.timestamps
    end
  end
end
