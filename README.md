# Challenge Week 5

## Store WEB coding challenge

This challenge is designed to put your skills to the test designing and building an app to manage a small snacks store. You can reuse the past weeks code, create views to render information.
You will need to add extra fields or tables on your database schema to match with the requirements

## DB conditions

- A product can have one or multiple comments each comment have a user, date and description
- An order can have one or multiple comments, each comment have a user, date and description
You can use a polymorphic table for the steps above
- A product can have one or multiple tags, as well a tag can have one or multiple products related to it.

## Requirements

The app should allow:

- Implement the best practices that you have learn
- Add rubocop
- Add a rating to the comment
- Make sure the app includes test with either RSpec or Minitest.
- Documentation with YARD or RDoc (Include instructions on how to see the result)
- Upload your solution to Heroku.

<hr/>


# How to use
1. Clone the project

			git clone https://gitlab.com/EdwinAlmira/trainee-week-5.git
2. Install the gems

			bundle install
3. Create the ENV variables for the postgres configuration. If you change the host, port or other db things need to edit the config/database.yml
			
			ENV['PG_USERNAME']			
			ENV['PG_PASS']
			
4. Setup the database

	a. Create DB

			rake db:create
	b. Make the migration

			rake db:migrate
	c. Populate the database with example records

			rake db:seed
5. Setup boostrap 5
		
			yarn add bootstrap@5.1.2
6. Install Popperjs and select verssion 5.1.2

			yarn add bootstrap@next jquery @popperjs/core

	- If you have problems with an old popper instalation, just run this command and repeat the 6 step

			yarn remove bootstrap jquery popper.js

7. Remove compiled assets and make a new compile with webpacker
		
		bin/rails assets:clobber  
		bin/rails webpacker:compile  

7. Start the server
	
			bin/rails s

## Default users accounts
<hr>


### Admin
- Email: admin@site.com
- Password: adminpass

### Customer
- Email: test@site.com
- Password: testpass

## What can be done?
<hr>

### Admin
- Add and remove products
- Update price and stock
- View all purchases
- View all products
- Create tags
- Add tags

### Customers
- Buy a product
- View own purchases
- Like and unlike a product
- View all product
- Add product to the cart
- Comment products
- Comments his own purchases

### Everyone
- View all products
- Order by popularity and name
- Filter by tag name

## Documentation
<hr>

### Used YARD for Documentation.
- You can see the documentation in the trainee-week-5/doc/index.html file 

![logo](https://cdn-assets-cloud.frontify.com/s3/frontify-cloud-files-us/eyJwYXRoIjoiZnJvbnRpZnlcL2FjY291bnRzXC9kYlwvMTcwODAwXC9wcm9qZWN0c1wvMjA4ODIwXC9hc3NldHNcLzA3XC80NTQ2OTk2XC9mY2UwOTQ5ZjhkMzZhMWRhNmQ5YzYyMWE2Zjg3YjEwYS0xNTk0ODYwMDIzLnBuZyJ9:frontify:mJqQOmVZ9oqFYzejiZy5xX9TWc5XwEbLlGNN7-ooJ7s?width=200)
