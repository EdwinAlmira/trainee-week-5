# frozen_string_literal: true

# Builder for Products Json
json.product do
  json.call(@product, :name, :description, :price, :stock, :created_at)
  json.old_price @product.price_changes.last.old_price
  json.likes @product.likes.count
end

json.comments @product.comments, :user_id, :description, :created_at
