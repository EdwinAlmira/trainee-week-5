# frozen_string_literal: true

module Purchases
  class PurchasePresenter < Representable::Decorator
    include Representable::JSON

    property :quantity
    property :created_at
    property :status
    property :product do
      property :name
      property :price
    end

    collection :comments, decorator: CommentPresenter, class: Comment
  end
end
