# frozen_string_literal: true

module Products
  class ProductPresenter < Representable::Decorator
    include Representable::JSON

    property :name
    property :price
    property :stock
    property :description
    property :popularity
    collection :tags, decorator: TagPresenter, class: Tag
  end
end
