# frozen_string_literal: true

module Users
  class AuthenticationPresenter < Representable::Decorator
    include Representable::JSON

    property :access_token
    property :expires
  end
end
