# frozen_string_literal: true

module Likes
  class LikePresenter < Representable::Decorator
    include Representable::JSON

    property :product_id
    property :user_id
  end
end
