# frozen_string_literal: true

class CommentPresenter < Representable::Decorator
  include Representable::JSON

  property :created_at
  property :description
end
