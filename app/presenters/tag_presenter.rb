# frozen_string_literal: true

class TagPresenter < Representable::Decorator
  include Representable::JSON

  property :tag_name
end
