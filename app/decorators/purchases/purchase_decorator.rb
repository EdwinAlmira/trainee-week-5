# frozen_string_literal: true

module Purchases
  class PurchaseDecorator < SimpleDelegator
    def formatted_date
      cdate.strftime('%d-%m-%Y')
    end
  end
end
