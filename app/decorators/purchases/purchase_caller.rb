# frozen_string_literal: true

module Purchases
  class PurchaseCaller
    def initialize(purchase)
      @purchase = purchase
    end

    def comment_description
      @purchase.comments.first.description
    end

    def product_stock
      @purchase.product.stock
    end

    def purchase_total
      @purchase.money * @purchase.quanti
    end
  end
end
