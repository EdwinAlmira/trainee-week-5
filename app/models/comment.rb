# frozen_string_literal: true

# Polymorphic model who store comments for purchases or products.
# Have a scope who returns if a purchases had a comment
class Comment < ApplicationRecord
  belongs_to :commented, polymorphic: true
  belongs_to :user

  scope :purchase_already_commented, lambda { |purchase_id|
    where("commented_id = ? AND commented_type = 'Purchase'", purchase_id)
  }
end
