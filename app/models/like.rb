# frozen_string_literal: true

# Many to many model between users and products that save if a user likes a product.
# Also have a scope query who returns if a user already like a product
class Like < ApplicationRecord
  belongs_to :product
  belongs_to :user

  scope :already_liked, lambda { |user_id, product_id|
    select('COUNT(id) as likes').where(user_id: user_id, product_id: product_id)[0].likes
  }
end
