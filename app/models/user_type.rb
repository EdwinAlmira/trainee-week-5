# frozen_string_literal: true

# Class for store and manage the types of user
# by default only have two users:
# 1. Admin
# 2. Customer
class UserType < ApplicationRecord
  has_many :users
end
