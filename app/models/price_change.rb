# frozen_string_literal: true

# Model who store the price changes in a product. Have 2 attributes:
# 1. Product_id
# 2. old_price
# Also have a scope query to return if a product had a price changes
class PriceChange < ApplicationRecord
  belongs_to :product

  scope :did_the_price_change, lambda { |product_id|
    where(product_id: product_id).order(created_at: :desc)
  }
end
