# frozen_string_literal: true

# Many to many model between tags and products.
# Have the scope query for know if a product has already tagged with a specific tag
class ProductTag < ApplicationRecord
  belongs_to :tag
  belongs_to :product

  scope :already_tagged, ->(product_id, tag_id) { where(product_id: product_id, tag_id: tag_id) }
end
