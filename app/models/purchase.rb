# frozen_string_literal: true

# Purchases its like a order model, store the active, cancel and pending orders.
# Have 4 attibutes:
# 1. Cart_id
# 2. Product_id
# 3. Quantity
# 4. status
# The purchases its the many to many table between products and carts
class Purchase < ApplicationRecord
  belongs_to :product
  belongs_to :cart
  has_many :comments, as: :commented
  enum status: %i[pending cancel paid]

  delegate :product_stock, :comment_description, :purchase_total, to: :caller
  def caller
    Purchases::PurchaseCaller.new(self)
  end

  scope :already_in_cart, lambda { |product_id, cart_id|
    where('status = 0 AND product_id = ? AND cart_id = ?', product_id, cart_id)
  }
  scope :buy_purchases, ->(cart_id) { where(status: :pending, cart_id: cart_id).update_all(status: 2) }
end
