# frozen_string_literal: true

# Carts its the model where the apps store the orders.
# Have 4 attibutes:
# 1. User_id
# 2. Purchase_id
# 3. Total
# 4. status
# Has_cart_active its a query who returns the active cart for a user
class Cart < ApplicationRecord
  belongs_to :user

  has_many :purchases do
    def active
      where('status = 0')
    end
  end

  has_many :products, through: :purchases
  enum status: %i[pending cancel paid]

  scope :has_cart_active, lambda { |user_id|
    where(status: :pending, user_id: user_id)
  }
end
