# frozen_string_literal: true

# The product model have four attributes
# 1. Name
# 2. Price
# 3. Stock
# 4. Description
# Every attribute has his validation
# The class have a scope method for update the product stock after a purchase

class Product < ApplicationRecord
  has_many :purchases
  has_many :carts, through: :purchases
  has_many :likes
  has_many :product_tags
  has_many :price_changes
  has_many :tags, through: :product_tags
  has_many :users, through: :likes
  has_many :comments, as: :commented
  has_one_attached :picture

  # Update the stock of the buyed products
  scope :buy_product, lambda { |product_id, new_stock|
    find_by(id: product_id).update(stock: new_stock)
  }
end
