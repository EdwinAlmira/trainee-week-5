# frozen_string_literal: true

class UserReview < ApplicationRecord
  belongs_to :reviewer, class_name: 'User'
  belongs_to :reviewed, class_name: 'User'
  enum status: %i[pending approved]
end
