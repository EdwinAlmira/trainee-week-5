# frozen_string_literal: true

# Model created with devise gem
class User < ApplicationRecord
  has_many :likes
  has_many :carts
  has_many :comments
  has_many :purchases, through: :carts
  has_many :products, through: :likes
  has_many :reviews_made, class_name: 'UserReview', foreign_key: :reviewer_id
  has_many :reviews_received, class_name: 'UserReview', foreign_key: :reviewed_id

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :email, presence: true
  belongs_to :user_type
  has_many :carts

  def active_reviews
    reviews_received.where(status: :approved)
  end
end
