# frozen_string_literal: true

# Tags can be assigned to a product for make more easy his search. Has only one attribute
# 1. Tag name
class Tag < ApplicationRecord
  has_many :product_tags
end
