# frozen_string_literal: true

module ErrorHandler
  def self.included(clazz)
    clazz.class_eval do
      rescue_from ActiveRecord::RecordInvalid do |e|
        respond(:record_invalid, 422, e.to_s)
      end
      rescue_from CustomError do |e|
        respond(e.error, e.status, e.message)
      end
      rescue_from JWT::DecodeError do |e|
        respond(:invalid_token, 401, e.to_s)
      end
      rescue_from ActiveRecord::InvalidForeignKey do |e|
        respond(:invalid_deletion, 401, e.to_s)
      end
      rescue_from Pagy::OverflowError do |e|
        respond(:page_overflow, 406, e.to_s)
      end
      rescue_from ActionView::Template::Error do |e|
        respond(:template, 206, e.to_s)
      end
    end
  end

  private

  def respond(error, status, message)
    json = { error: error, message: message }
    render json: json, status: status
  end
end
