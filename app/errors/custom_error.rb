# frozen_string_literal: true

class CustomError < StandardError
  attr_reader :error, :message, :status

  def initialize(error = 'base', message = 'something going wrong', status = nil)
    @error = error
    @message = message
    @status = status
  end

  def fetch_json
    Helpers::Render.json(error, message)
  end
end
