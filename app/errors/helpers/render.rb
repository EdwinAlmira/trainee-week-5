# frozen_string_literal: true

module Helpers
  class Render
    def self.json(error, message)
      { error: error, message: message }
    end
  end
end
