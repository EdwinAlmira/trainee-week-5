# frozen_string_literal: true

module Tags
  class TagQuery
    attr_reader :relation

    def initialize(relation = Tag.all)
      @relation = relation
    end

    def order_by_date
      @relation.order('tags.created_at desc')
    end
  end
end
