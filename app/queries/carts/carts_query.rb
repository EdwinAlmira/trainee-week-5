# frozen_string_literal: true

module Carts
  class CartsQuery
    attr_reader :carts

    def initialize(carts = Cart.joins(:user, purchases: [:product])
                               .includes(:products, :purchases))
      @carts = carts
    end

    def select_cart(cart_id)
      @carts.carts.find_by(id: cart_id)
    end
  end
end
