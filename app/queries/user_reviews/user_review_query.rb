# frozen_string_literal: true

module UserReviews
  class UserReviewQuery
    attr_reader :reviews

    def initialize(reviews = UserReview.joins(:reviewer, :reviewed).includes(:reviewer, :reviewed))
      @reviews = reviews
    end
  end
end
