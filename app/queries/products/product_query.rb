# frozen_string_literal: true

module Products
  class ProductQuery
    attr_reader :products

    def initialize(products = Product.joins(:picture_attachment).left_outer_joins(:product_tags, :tags)
                                     .includes(:picture_attachment, :tags, :product_tags)
                                     .select('products.*'))
      @products = products
    end

    def order_by(attribute, order)
      @products.order("products.#{attribute} #{order}")
    end

    def filter_tag(tag)
      if tag.empty?
        @products
      else
        @products.where("tags.tag_name LIKE '%#{tag}%'")
      end
    end
  end
end
