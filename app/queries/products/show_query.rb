# frozen_string_literal: true

module Products
  class ShowQuery
    attr_reader :product

    def initialize(products = Product.left_outer_joins(:picture_attachment, :likes, :product_tags, :tags, :price_changes)
                                     .includes(:picture_attachment, :price_changes, :likes, :product_tags, :tags))
      @products = products
    end

    def find_by_user(user_id)
      @products.find_by(id: user_id)
    end
  end
end
