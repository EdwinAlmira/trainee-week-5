# frozen_string_literal: true

module Purchases
  class PurchaseQuery
    attr_reader :purchases

    def initialize(purchases = Purchase.joins(:product, :cart)
                                       .left_outer_joins(:comments)
                                       .select('purchases.id as pid, products.name as name_product,
                                              products.price as money, purchases.quantity as quanti,
                                              purchases.created_at as cdate'))
      @purchases = purchases
    end

    def order_by_date
      @purchases.order('purchases.created_at desc')
    end

    def by_user(user)
      @purchases.where('carts.user_id = ?', user.id)
    end

    def paid_purchases
      @purchases.where(status: :paid)
    end
  end
end
