# frozen_string_literal: true

module Products
  class FilterParams < BaseForm
    attr_accessor :order, :attribute, :filter_tag, :page, :size

    validates :order, inclusion: { in: %w[asc desc], message: 'Order is invalid. Only accept asc or desc as order.' }
    validates :attribute, inclusion: { in: %w[name popularity],
                                       message: 'Attribute is invalid. Only accept name or popularity as attribute.' }
    validates :page, numericality: { only_integer: true }
    validates :size, numericality: { only_integer: true }
    validates :size, numericality: { greater_than: 0, message: 'The size value need to be greater than 0' }

    def validate!
      raise CustomError.new('Params error', errors.messages, 400), self unless valid?

      FilterParams.new(order: order, attribute: attribute, filter_tag: filter_tag, size: size)
    end
  end
end
