# frozen_string_literal: true

module Products
  class DeleteForm < BaseForm
    attr_accessor :id

    validates :id, presence: { message: 'The product need a price' }
    validate :product_exist

    def validate!
      raise CustomError.new('Creating product error', errors.messages, 400), self unless valid?

      DeleteForm.new(id: id)
    end

    def product_exist
      product = Product.find_by(id: id)
      errors.add(:product_id, 'Product not exists') if product.nil?
    end
  end
end
