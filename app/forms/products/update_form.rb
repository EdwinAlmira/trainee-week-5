# frozen_string_literal: true

module Products
  class UpdateForm < BaseForm
    attr_accessor :product_id, :price, :stock

    validates :price, numericality: { greater_than: 0, message: 'The price value need to be greater than 0' }
    validates :stock, numericality: { greater_than: 0, message: 'The stock value   qneed to be greater than 0' }
    validates :price, presence: { message: 'The product need a price' }
    validates :stock, presence: { message: 'The product need to have a stock' }
    validates :product_id, presence: { message: 'Need a product to update' }
    validate :product_exist

    def validate!
      raise CustomError.new('Creating product error', errors.messages, 400), self unless valid?

      UpdateForm.new(product_id: product_id, price: price, stock: stock)
    end

    def product_exist
      product = Product.find_by(id: product_id)
      errors.add(:product_id, 'Product not exists') if product.nil?
    end
  end
end
