# frozen_string_literal: true

module Products
  class ProductAddForm < BaseForm
    attr_accessor(
      :name,
      :price,
      :stock,
      :description,
      :picture
    )

    validates :price, numericality: { greater_than: 0, message: 'The price value need to be greater than 0' }
    validates :stock, numericality: { greater_than: 0, message: 'The stock value   qneed to be greater than 0' }
    validates :name, presence: { message: 'The product need a name' }
    validates :price, presence: { message: 'The product need a price' }
    validates :stock, presence: { message: 'The product need to have a stock' }
    validates :picture, presence: { message: 'The product need a picture' }
    # validates :picture, attached: true, content_type: { in: ['image/png', 'image/jpg', 'image/jpeg'], message: 'Picture invalid o void' }

    def validate!
      raise CustomError.new('Creating product error', errors.messages, 400), self unless valid?

      ProductAddForm.new(name: name, price: price, stock: stock, description: description, picture: picture)
    end
  end
end
