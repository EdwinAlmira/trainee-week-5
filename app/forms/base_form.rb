# frozen_string_literal: true

class BaseForm
  include ActiveModel::Model
  # include ActiveRecord::Validations
  include ActiveStorageValidations
end
