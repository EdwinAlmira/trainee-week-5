# frozen_string_literal: true

module Likes
  class LikeForm < BaseForm
    attr_accessor :user_id, :product_id

    validates :user_id, presence: { message: 'Need a user' }
    validates :product_id, presence: { message: 'Need a product' }
    validate :product_exist

    def validate!
      raise CustomError.new('Invalid params for liking', errors.messages, 400), self unless valid?

      LikeForm.new(user_id: user_id, product_id: product_id)
    end

    private

    def product_exist
      product = Product.find_by(id: product_id)
      errors.add(:product_id, 'product not exists') if product.nil?
    end
  end
end
