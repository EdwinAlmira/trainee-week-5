# frozen_string_literal: true

module Purchases
  class DeleteForm < BaseForm
    attr_accessor :id

    validates :id, presence: { message: 'Need a purchase' }
    validate :purchase_exist

    def validate!
      raise CustomError.new('Invalid params for destroy a purchase', errors.messages, 400), self unless valid?

      DeleteForm.new(id: id)
    end

    private

    def purchase_exist
      purchase = Purchase.find_by(id: id)
      errors.add(:purchase_id, 'Purchase not exists') if purchase.nil?
      purchase_pending(purchase)
    end

    def purchase_pending(purchase)
      errors.add(:purchase_id, 'Purchase is not active') if purchase.status != 'pending'
    end
  end
end
