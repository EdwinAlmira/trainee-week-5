# frozen_string_literal: true

module Purchases
  class PurchaseForm < BaseForm
    attr_accessor :cart_id, :product_id, :quantity

    validates :cart_id, presence: { message: 'Need a user' }
    validates :quantity, presence: { message: 'How much you want of this product?' }
    validates :quantity, numericality: { only_integer: true, message: 'Need to be a number' }
    validates :quantity, numericality: { greater_than: 0, message: 'The quantity value need to be greater than 0' }
    validates :product_id, presence: { message: 'Need a product' }
    validate :product
    validate :cart_active

    def validate!
      raise CustomError.new('Invalid params for make a purchase', errors.messages, 400), self unless valid?

      PurchaseForm.new(cart_id: cart_id, product_id: product_id, quantity: quantity)
    end

    private

    def product
      product = Product.find_by(id: product_id)
      if product.nil?
        errors.add(:product_id, 'product not exists')
      else
        quantity_no_more_than_stock
        already_in_cart
      end
    end

    def quantity_no_more_than_stock
      stock = Product.find_by(id: product_id).stock

      errors.add(:quantity, 'The quantity has to be less or equal than the product stock') if quantity.to_i > stock
    end

    def cart_active
      cart = Cart.find_by(id: cart_id)
      errors.add(:cart_id, 'The cart its not active') if cart.status != 'pending'
    end

    def already_in_cart
      errors.add(:product_id, 'Product already in the cart') unless Purchase.already_in_cart(product_id, cart_id).empty?
    end
  end
end
