# frozen_string_literal: true

module Comments
  class CommentPurchaseForm < BaseForm
    attr_accessor :purchase_id, :description, :user_id

    validates :user_id, presence: { message: 'Need a user' }
    validates :description, presence: { message: 'The comment need a description' }
    validates :description, length: { maximum: 300, message: 'The comment exceed the 300 characters' }
    validates :purchase_id, presence: { message: 'Need a purchase to comment' }
    validate :purchase_exist
    validate :user_exist

    def validate!
      raise CustomError.new('Invalid params for make a purchase', errors.messages, 400), self unless valid?

      CommentPurchaseForm.new(purchase_id: purchase_id, description: description, user_id: user_id)
    end

    private

    def purchase_exist
      purchase = Purchase.find_by(id: purchase_id)
      errors.add(:purchase_id, 'Purchase not exists') if purchase.nil?
    end

    def user_exist
      user = User.find_by(id: user_id)
      errors.add(:purchase_id, 'User not exists') if user.nil?
    end
  end
end
