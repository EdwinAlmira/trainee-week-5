# frozen_string_literal: true

module Comments
  class CommentProductForm < BaseForm
    attr_accessor :product_id, :description, :user_id

    validates :user_id, presence: { message: 'Need a user' }
    validates :description, presence: { message: 'The comment need a description' }
    validates :description, length: { maximum: 300, message: 'The comment exceed the 300 characters' }
    validates :product_id, presence: { message: 'Need a product to comment' }
    validate :product_exist
    validate :user_exist

    def validate!
      raise CustomError.new('Invalid params for make a comment', errors.messages, 400), self unless valid?

      CommentProductForm.new(product_id: product_id, description: description, user_id: user_id)
    end

    private

    def product_exist
      product = Product.find_by(id: product_id)
      errors.add(:product_id, 'Product not exists') if product.nil?
    end

    def user_exist
      user = User.find_by(id: user_id)
      errors.add(:User_id, 'User not exists') if user.nil?
    end
  end
end
