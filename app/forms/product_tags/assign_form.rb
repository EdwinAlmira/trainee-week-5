# frozen_string_literal: true

module ProductTags
  class AssignForm < BaseForm
    attr_accessor :product_id, :tag_id

    validates :tag_id, presence: { message: 'Need a user' }
    validates :product_id, presence: { message: 'Need a product' }
    validate :product_exist
    validate :tag_exist
    validate :tagged

    def validate!
      raise CustomError.new('Invalid params for liking', errors.messages, 400), self unless valid?

      AssignForm.new(tag_id: tag_id, product_id: product_id)
    end

    private

    def product_exist
      product = Product.find_by(id: product_id)
      errors.add(:product_id, 'product not exists') if product.nil?
    end

    def tag_exist
      tag = Tag.find_by(id: tag_id)
      errors.add(:tag_id, 'product not exists') if tag.nil?
    end

    def tagged
      tag = ProductTag.already_tagged(product_id, tag_id)
      errors.add(:tag_id, 'product already tagged') unless tag.empty?
    end
  end
end
