# frozen_string_literal: true

module Carts
  class UpdateForm < BaseForm
    attr_accessor :cart_id, :total

    validates :cart_id, presence: { message: 'Need a cart' }
    validates :total, presence: { message: 'Need a total' }
    validates :total, numericality: { greater_than: 0, message: 'The total value need to be greater than 0' }
    validate :cart_exist

    def validate!
      raise CustomError.new('Invalid params when try pay your cart', errors.messages, 400), self unless valid?

      UpdateForm.new(cart_id: cart_id, total: total)
    end

    private

    def cart_exist
      cart = Cart.find_by(id: cart_id)
      errors.add(:cart_id, 'Cart not exists') if cart.nil?
    end
  end
end
