# frozen_string_literal: true

module Carts
  class AddForm < BaseForm
    attr_accessor :status, :user_id, :total

    validates :user_id, presence: { message: 'Need a user' }

    def validate!
      raise CustomError.new('Invalid params for make a cart', errors.messages, 400), self unless valid?

      AddForm.new(user_id: user_id)
    end
  end
end
