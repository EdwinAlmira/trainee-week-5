# frozen_string_literal: true

module UserReviews
  class ReviewForm < BaseForm
    attr_accessor :status, :reviewed_id, :reviewer_id, :review

    validates :reviewed_id, presence: { message: 'Need a user' }
    validates :reviewer_id, presence: { message: 'Need a user' }
    validates :status, presence: { message: 'Need a user' }
    validates :review, presence: { message: 'How much you want of this product?' }
    validates :review, length: { maximum: 300, message: 'The comment exceed the 300 characters' }
    validate :reviewed_exist
    validate :reviewer_exist
    validate :own_review

    def validate!
      raise CustomError.new('Invalid params for make a review', errors.messages, 400), self unless valid?

      ReviewForm.new(status: status, reviewed_id: reviewed_id, reviewer_id: reviewer_id, review: review)
    end

    private

    def reviewed_exist
      user = User.find_by(id: reviewed_id)
      errors.add(:reviewed_id, 'User reviewed not exists') if user.nil?
    end

    def reviewer_exist
      user = User.find_by(id: reviewer_id)
      errors.add(:reviewer_id, 'User reviewer not exists') if user.nil?
    end

    def own_review
      errors.add(:invalid_review, 'Users cannot review themselves') if reviewed_id == reviewer_id
    end
  end
end
