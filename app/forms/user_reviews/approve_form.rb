# frozen_string_literal: true

module UserReviews
  class ApproveForm < BaseForm
    attr_accessor :id

    validates :id, presence: { message: 'Need a review' }

    def validate!
      raise CustomError.new('Invalid params for change status for a review', errors.messages, 400), self unless valid?

      ApproveForm.new(id: id)
    end

    private

    def review_exist
      review = UserReview.find_by(id: id)
      errors.add(:Review, 'Review not exists') if review.nil?
    end
  end
end
