# frozen_string_literal: true

module Tags
  class TagRegistration < BaseForm
    attr_accessor :tag_name

    validates :tag_name, presence: { message: 'The tag name need a value' }
    validate :unique_tag

    def validate!
      raise CustomError.new('Invalid params for creating a tag', errors.messages, 400), self unless valid?

      TagRegistration.new(tag_name: tag_name)
    end

    private

    def unique_tag
      tag = Tag.find_by(tag_name: tag_name)
      errors.add(:name, 'name already exists') unless tag.nil?
    end
  end
end
