# frozen_string_literal: true

module Tags
  class DeleteForm < BaseForm
    attr_accessor :id

    validates :id, presence: { message: 'The tag name need a value' }
    validates :id, numericality: { only_integer: true, message: 'Need to be a number' }
    validate :tag_exist

    def validate!
      raise CustomError.new('Invalid params for delete a tag', errors.messages, 400), self unless valid?

      DeleteForm.new(id: id)
    end

    private

    def tag_exist
      tag = Tag.find_by(id: id)
      errors.add(:tag_id, 'Cant find the tag') if tag.nil?
    end
  end
end
