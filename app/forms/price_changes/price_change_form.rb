# frozen_string_literal: true

module PriceChanges
  class PriceChangeForm < BaseForm
    attr_accessor :product_id, :old_price, :new_price

    validates :product_id, presence: { message: 'Need a product' }
    validates :old_price, presence: { message: 'Need to have a old price' }
    validates :old_price, numericality: { greater_than: 0, message: 'The old price value need to be greater than 0' }
    validates :new_price, presence: { message: 'Need to have a new price' }
    validates :new_price, numericality: { greater_than: 0, message: 'The old price value need to be greater than 0' }
    validate :product_exist

    def validate!
      raise CustomError.new('Invalid params for price changing', errors.messages, 400), self unless valid?

      PriceChangeForm.new(product_id: product_id, old_price: old_price, new_price: new_price)
    end

    private

    def product_exist
      product = Product.find_by(id: product_id)
      errors.add(:product_id, 'product not exists') if product.nil?
    end
  end
end
