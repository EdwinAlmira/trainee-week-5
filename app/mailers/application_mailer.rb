# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'elsarco12385@gmail.com'
  layout 'mailer'
end
