# frozen_string_literal: true

class ProductMailer < ApplicationMailer
  def run_out_of_stock
    @user = params[:user]
    @product = params[:product]
    @filename = @product.picture.filename.to_s
    attachments[@filename] = @product.picture.download
    mail(to: @user.email, subject: 'This product its about to finish')
  end
end
