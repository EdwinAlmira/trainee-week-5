# frozen_string_literal: true

module Api
  module V1
    # Controller to allow user to login in the API and return his token and when
    # the token expires
    class AuthenticationController < ApplicationController
      # POST /api/v1/auth/login
      def login
        auth = Users::Auth.call(login_params)
        render json: auth, status: :ok
      end

      private

      def login_params
        params.permit(:email, :password)
      end
    end
  end
end
