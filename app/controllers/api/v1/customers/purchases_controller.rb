# frozen_string_literal: true

module Api
  module V1
    module Customers
      # Create a purchase and assigned a cart.
      # Get product_id and quantity params.
      # quantity > 0
      class PurchasesController < ApplicationController
        before_action :bearer_token
        before_action :authorize_request

        def index
          purchases = Purchases::List.call(@token)
          render json: purchases, status: :ok
        end

        def create
          purchase = Purchases::AddToCart.call(product_id: purchase_params[:product_id],
                                               quantity: purchase_params[:quantity], token: @token)
          render json: purchase, status: :ok
        end

        private

        def purchase_params
          params.permit(:product_id, :quantity)
        end
      end
    end
  end
end
