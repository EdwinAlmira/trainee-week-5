# frozen_string_literal: true

# Father application controller. Have the devise filters
module Api
  module V1
    module Customers
      # Include the pagy backend
      class ApplicationController < Api::V1::ApplicationController
        include Pagy::Backend
      end
    end
  end
end
