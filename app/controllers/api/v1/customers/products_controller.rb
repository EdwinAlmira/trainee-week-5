# frozen_string_literal: true

module Api
  module V1
    module Customers
      # Controller to allow acces to list products
      # order -> Only accept 'asc' o 'desc'
      # attribute -> Only accept 'name' o 'popularity'
      # filter_tag -> String
      # page -> Only accept integer and depend on how much products are
      # size -> Only accept integer
      class ProductsController < ApplicationController
        def index
          render json: Products::List.call(products_params), status: :ok
        end

        private

        def products_params
          params.permit(:order, :filter_tag, :attribute, :page, :size)
        end
      end
    end
  end
end
