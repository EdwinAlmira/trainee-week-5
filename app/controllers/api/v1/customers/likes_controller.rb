# frozen_string_literal: true

module Api
  module V1
    module Customers
      # Save likes and if a product is already liked for a user
      # this products its disliked.
      # Recive product_id to perform the action
      class LikesController < ApplicationController
        before_action :bearer_token
        before_action :authorize_request

        def create
          like = Likes::AddLike.call(product_id: like_params[:product_id], token: @token)
          render json: like, status: :ok
        end

        private

        def like_params
          params.permit(:product_id)
        end
      end
    end
  end
end
