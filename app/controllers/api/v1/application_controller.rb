# frozen_string_literal: true

module Api
  module V1
    # Controller where return a json when the url not found and
    # And the method of authorization
    class ApplicationController < Api::ApplicationController
      before_action :bearer_token

      def not_found
        render json: { error: 'not_found' }
      end

      def authorize_request
        @decoded = JsonWebToken.decode(@token)
        @current_user = User.find(@decoded[:user_id])
      rescue ActiveRecord::RecordNotFound => e
        render json: { errors: e.message }, status: :unauthorized
      rescue JWT::DecodeError => e
        render json: { errors: e.message }, status: :unauthorized
      end
    end
  end
end
