# frozen_string_literal: true

module Api
  #  Father controller in API. Include the module who catch the errors and
  #  the bearer_token method who process the bearer token to get only the token
  class ApplicationController < ActionController::API
    include ErrorHandler

    def bearer_token
      pattern = /^Bearer /
      header  = request.headers['Authorization']
      @token = header.gsub(pattern, '') if header&.match(pattern)
    end
  end
end
