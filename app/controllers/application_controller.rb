# frozen_string_literal: true

# Father application controller. Have the devise filters
class ApplicationController < ActionController::Base
  include ErrorHandler
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def its_customer?
    unless current_user.user_type_id == 2
      redirect_to inventory_products_path, notice: 'You must be logged as customer to access this section'
    end
  end

  def its_admin?
    unless current_user.user_type_id == 1
      redirect_to inventory_products_path, notice: 'You must be logged as admin to access this section'
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name email password user_type_id])
  end
end
