# frozen_string_literal: true

module Inventory
  # Only purpose is return a user info
  class UsersController < ApplicationController
    def show
      @user = User.find_by(id: params[:id])
    end
  end
end
