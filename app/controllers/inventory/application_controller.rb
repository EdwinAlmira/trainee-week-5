# frozen_string_literal: true

# Inherits from the main Application controller and have protected method to filter types of users

module Inventory
  class ApplicationController < ApplicationController
  end
end
