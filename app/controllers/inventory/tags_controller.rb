# frozen_string_literal: true

module Inventory
  # Tags actions are limited for only admin use
  class TagsController < ApplicationController
    before_action :authenticate_user!
    before_action :its_admin?

    def index
      @tags = Tags::ReturnTags.call
    end

    def new
      @tag = Tag.new
    end

    # Tag_params its a filter who only permit tag_name as a parameter
    def create
      tag = Tags::Saver.call(tag_params)
      redirect_to inventory_tags_path, notice: tag
    end

    # Return a notice if the tag have a association restriction
    def destroy
      @tag = Tags::Delete.call(id: params[:id])
      redirect_to inventory_tags_path, notice: @tag
    end

    private

    def tag_params
      params.require(:tag).permit(:tag_name)
    end
  end
end
