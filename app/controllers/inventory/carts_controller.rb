# frozen_string_literal: true

module Inventory
  # Products actions are limited by user type, only customer can make this actions
  class CartsController < ApplicationController
    before_action :authenticate_user!
    before_action :its_customer?

    def show
      @cart = Carts::Show.call(cart_params[:id])
    end

    def update
      cart = Carts::Pay.call(cart_params)
      redirect_to inventory_products_path, notice: cart
    end

    private

    def cart_params
      params.permit(:id, :total)
    end
  end
end
