# frozen_string_literal: true

module Inventory
  # Product tag controller dont have a view
  class ProductTagsController < ApplicationController
    before_action :authenticate_user!
    before_action :its_admin?
    # Check if the product had the tag and if not, add the tag to the product
    def create
      product_tag = ProductTags::AssignTag.call(product_tag_params)
      redirect_to inventory_products_path, notice: product_tag
    end

    private

    def product_tag_params
      params.permit(:tag_id, :product_id)
    end
  end
end
