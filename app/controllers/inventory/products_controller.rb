# frozen_string_literal: true

require 'json'
module Inventory
  # Products actions are limited by user type
  class ProductsController < Inventory::ApplicationController
    include Pagy::Backend
    before_action :authenticate_user!, except: %i[index show]
    before_action :its_admin?, except: %i[index show]

    def index
      @pagy, @products = pagy(Products::ProductQuery.new.products)
    end

    def show
      @product = Products::ShowQuery.new.find_by_user(params[:id])
      @purchase = Purchase.new
    end

    def new
      @product = Product.new
    end

    def create
      product = Products::Add.call(name: params[:product][:name], price: params[:product][:price],
                                   stock: params[:product][:stock], description: params[:product][:description],
                                   picture: params[:product][:picture])
      redirect_to inventory_products_path, notice: product
    end

    def edit
      @product = Product.find_by(id: params[:id])
    end

    def update
      product = Products::Modify.call(product_id: params[:id], stock: params[:stock], price: params[:price])
      redirect_to inventory_product_path, notice: product
    end

    def destroy
      product = Products::Delete.call(params[:id])
      redirect_to inventory_products_path, notice: product
    end

    private

    def product_params
      params.permit(:id, :name, :price, :stock, :description, :picture)
    end
  end
end
