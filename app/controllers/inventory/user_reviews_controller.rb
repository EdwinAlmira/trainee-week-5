# frozen_string_literal: true

module Inventory
  # Admins can update and destroy reviews while the user only can created
  # Reviews need to be approved before be public
  class UserReviewsController < ApplicationController
    before_action :authenticate_user!
    before_action :its_customer?, only: %i[create]
    before_action :its_admin?, except: %i[index create]

    def index
      @reviews = UserReviews::UserReviewQuery.new.reviews
    end

    def update
      review = UserReviews::Approve.call(review_params)
      redirect_to inventory_user_reviews_path, notice: review
    end

    def create
      review = UserReviews::Review.call(review_params)
      redirect_to inventory_products_path, notice: review
    end

    def destroy
      review = UserReviews::Delete.call(review_params)
      redirect_to inventory_user_reviews_path, notice: review
    end

    private

    def review_params
      params.permit(:id, :reviewed_id, :reviewer_id, :review)
    end
  end
end
