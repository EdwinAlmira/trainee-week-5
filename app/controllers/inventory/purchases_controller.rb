# frozen_string_literal: true

module Inventory
  # Products actions are limited by logged users
  class PurchasesController < ApplicationController
    before_action :authenticate_user!

    # The @purchase query make diferent joins if its a admin or a customer
    def index
      @purchases = Purchases::ListView.call(id: purchase_params[:id], user: current_user)
    end

    def show
      @purchase = Purchase.find_by(id: params[:id])
    end

    def new
      @purchase = Purchase.new
    end

    def create
      purchase = Purchases::AddToCartView.call(product_id: purchase_params[:product_id],
                                               quantity: purchase_params[:quantity], user_id: current_user.id)
      redirect_to inventory_products_path, notice: purchase
    end

    def destroy
      purchase = Purchases::Delete.call(purchase_params)
      redirect_to inventory_products_path, notice: purchase
    end

    private

    def purchase_params
      params.permit(:id, :product_id, :user_id, :quantity)
    end
  end
end
