# frozen_string_literal: true

module Store
  # Comments actions only for customers
  class CommentsController < ApplicationController
    before_action :authenticate_user!
    before_action :its_customer?

    def new
      @comment = Comment.new
    end

    def create
      comment = Comments::MakeAComment.call(comment_params)
      redirect_to inventory_products_path, notice: comment
    end

    private

    def comment_params
      params.permit(:type, :id, :user_id, :description)
    end
  end
end
