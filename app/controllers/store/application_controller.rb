# frozen_string_literal: true

module Store
  # Inherits from the main application controller. Dont have own actions
  class ApplicationController < ApplicationController
    def its_customer?
      unless current_user.user_type_id == 2
        redirect_to inventory_products_path,
                    notice: 'You must be logged as customer to access this section'
      end
    end
  end
end
