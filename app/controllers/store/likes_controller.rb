# frozen_string_literal: true

module Store
  # Likes actions are available for
  class LikesController < ApplicationController
    before_action :authenticate_user!
    before_action :its_customer?

    def new
      @like = Like.new
    end

    def create
      like = Likes::AddLikeView.call(product_id: like_params[:product_id], user_id: like_params[:user_id])
      redirect_to inventory_products_path, notice: like[:message]
    end

    def destroy
      like = Likes::RemoveLike.call(product_id: like_params[:product_id], user_id: like_params[:user_id])
      redirect_to inventory_products_path, notice: like[:message]
    end

    private

    def like_params
      params.permit(:user_id, :product_id)
    end
  end
end
