# frozen_string_literal: true

# Job used for notify a users where the products they like are run out of stock
module Products
  class RunOutOfStockJob < ApplicationJob
    queue_as :default

    def perform(cart)
      cart.products.each do |product|
        next unless product.stock <= 3

        liked_users = Like.left_outer_joins(:user).select('users.*').where(product_id: product.id)
        liked_users.each do |user|
          ProductMailer.with(user: user, product: product).run_out_of_stock.deliver_now
        end
      end
    end
  end
end
