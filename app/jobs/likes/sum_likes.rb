# frozen_string_literal: true

module Likes
  class SumLikes < ApplicationJob
    queue_as :default

    def perform(product_id)
      @product = Product.find_by(id: product_id)
      likes_quantity = @product.popularity + 1
      @product.update(popularity: likes_quantity)
    end
  end
end
