# frozen_string_literal: true

module Carts
  # Assign a cart to a purchase when the purchase is created
  # If the user have a cart active, this return that cart
  # if not, create a cart and return him
  class Assign < BaseService
    def initialize(user_id)
      @user_id = user_id
    end

    def call
      cart = validator
      select_cart(cart)
    end

    private

    def validator
      carts = Carts::AddForm.new(user_id: @user_id)

      carts.validate!
    end

    def select_cart(cart)
      Cart.create(user_id: cart.user_id) if Cart.has_cart_active(cart.user_id).empty?

      Cart.has_cart_active(cart.user_id).ids.first
    end
  end
end
