# frozen_string_literal: true

module Carts
  # Validate the attributes, and update a cart and the purchases if the cart update works
  class Pay < BaseService
    def initialize(cart_params)
      @cart_id = cart_params[:id]
      @total = cart_params[:total].to_f
    end

    def call
      cart = validator
      update_cart(cart)
    end

    private

    def validator
      carts = Carts::UpdateForm.new(cart_id: @cart_id, total: @total)
      carts.validate!
    end

    def update_cart(validate_params)
      cart = Cart.find_by(id: validate_params.cart_id)
      if cart.update(status: 2, total: validate_params.total)
        Carts::ReduceStock.call(cart)
        Purchase.buy_purchases(validate_params.cart_id)
        'You buy'
      else
        'Error'
      end
    end
  end
end
