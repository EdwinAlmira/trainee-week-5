# frozen_string_literal: true

module Carts
  # select the puchases in the cart and pass to a job who send a email if the product have
  # less than 3 unitys in his stock
  class ReduceStock
    class << self
      def call(cart)
        cart.purchases.active.each do |purchase|
          Product.buy_product(purchase.product_id, updated_stock(purchase))
        end
        Products::RunOutOfStockJob.perform_now(cart)
      end

      private

      def updated_stock(purchase)
        purchase.product_stock - purchase.quantity
      end
    end
  end
end
