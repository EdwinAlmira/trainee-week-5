# frozen_string_literal: true

module Carts
  # Return a cart
  class Show < BaseService
    def initialize(cart_id)
      @cart_id = cart_id
    end

    def call
      select_a_cart
    end

    private

    def select_a_cart
      all = CartsQuery.new
      CartsQuery.new(all).select_cart(@cart_id)
    end
  end
end
