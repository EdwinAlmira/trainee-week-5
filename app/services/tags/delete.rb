# frozen_string_literal: true

module Tags
  class Delete < BaseService
    attr_reader :tag

    def initialize(tag_params)
      @tag_id = tag_params[:id]
    end

    def call
      tag = validator
      destroy_tag(tag)
    end

    private

    def validator
      tag = Tags::DeleteForm.new(id: @tag_id)
      tag.validate!
    end

    def destroy_tag(tag_validated)
      tag = Tag.find_by(id: tag_validated.id)
      tag.destroy
      'Tag deleted'
    end
  end
end
