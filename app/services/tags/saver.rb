# frozen_string_literal: true

module Tags
  class Saver < BaseService
    attr_reader :tag

    def initialize(tag_params)
      @tag_params = tag_params.to_h
    end

    def call
      tag = validator
      save_tag(tag)
    end

    private

    def validator
      new_tag = Tags::TagRegistration.new(@tag_params)
      new_tag.validate!
    end

    def save_tag(tag_validated)
      tag = Tag.new(tag_name: tag_validated.tag_name)
      if tag.save
        'Tag added'
      else
        'Error to add tag'
      end
    end
  end
end
