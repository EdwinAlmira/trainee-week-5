# frozen_string_literal: true

module Tags
  class ReturnTags < BaseService
    def call
      get_tags
    end

    private

    def get_tags
      Tags::TagQuery.new.order_by_date
    end
  end
end
