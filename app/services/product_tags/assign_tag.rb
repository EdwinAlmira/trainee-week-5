# frozen_string_literal: true

module ProductTags
  class AssignTag < BaseService
    def initialize(product_tag_params)
      @product_id = product_tag_params[:product_id]
      @user_id = product_tag_params[:tag_id]
    end

    def call
      product_tag = validator
      save_product_tag(product_tag)
    end

    private

    def validator
      product_tag = ProductTags::AssignForm.new(tag_id: @user_id, product_id: @product_id)
      product_tag.validate!
    end

    def save_product_tag(validate_product_tag)
      product_tag = ProductTag.new(tag_id: validate_product_tag.tag_id, product_id: validate_product_tag.product_id)
      if product_tag.save
        'Tag added to the product'
      else
        'Cant assign the tag'
      end
    end
  end
end
