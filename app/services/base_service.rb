# frozen_string_literal: true

class BaseService
  include Pagy::Backend
  def self.call(*args)
    new(*args).call
  end
end
