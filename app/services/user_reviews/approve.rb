# frozen_string_literal: true

module UserReviews
  class Approve < BaseService
    def initialize(review_params)
      @review_id = review_params[:id]
      @status = :approved
    end

    def call
      approve_review(validator)
    end

    private

    def validator
      review = UserReviews::ApproveForm.new(id: @review_id)
      review.validate!
    end

    def approve_review(validated_params)
      review = UserReview.find_by(id: validated_params.id)
      raise CustomError.new('Review error', 'The review cant be approved', 400) unless review.update(status: @status)

      'Review approved'
    end
  end
end
