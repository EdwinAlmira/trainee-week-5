# frozen_string_literal: true

module UserReviews
  class Delete < BaseService
    def initialize(review_params)
      @review_id = review_params[:id]
    end

    def call
      delete_review(validator)
    end

    private

    def validator
      review = UserReviews::ApproveForm.new(id: @review_id)
      review.validate!
    end

    def delete_review(validated_params)
      review = UserReview.find_by(id: validated_params.id)
      raise CustomError.new('Review error', 'The review cant be deleted', 400) unless review.destroy

      'Review deleted'
    end
  end
end
