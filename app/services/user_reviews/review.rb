# frozen_string_literal: true

module UserReviews
  class Review < BaseService
    def initialize(review_params)
      @reviewed_id = review_params[:reviewed_id]
      @reviewer_id = review_params[:reviewer_id]
      @review = review_params[:review]
      @status = :pending
    end

    def call
      save_review(validator)
    end

    private

    def validator
      review = UserReviews::ReviewForm.new(status: @status, reviewed_id: @reviewed_id, reviewer_id: @reviewer_id,
                                           review: @review)
      review.validate!
    end

    def save_review(validated_params)
      review = UserReview.new(status: validated_params.status, reviewed_id: validated_params.reviewed_id,
                              reviewer_id: validated_params.reviewer_id, review: validated_params.review)
      raise CustomError.new('Review error', 'The review cant be saved', 400) unless review.save

      'Review sended. Pending of admin aprovation.'
    end
  end
end
