# frozen_string_literal: true

require 'ostruct'

module Users
  class Auth < BaseService
    def initialize(auth_params)
      @email = auth_params[:email]
      @pass = auth_params[:password]
    end

    def call
      auth = validate_password(get_user)
      auth = OpenStruct.new(auth)
      Users::AuthenticationPresenter.new(auth)
    end

    private

    def validate_password(user)
      return build_token(user) if user&.valid_password?(@pass)

      raise CustomError.new('Password error', 'You are using a wrong password', 401)
    end

    def get_user
      user = User.find_by(email: @email.downcase)
      return user unless user.nil?

      raise CustomError.new('Email error', 'The email dont exist', 401)
    end

    def build_token(user)
      token = JsonWebToken.encode(user_id: user.id)
      time = Time.now + 24.hours.to_i
      { access_token: token, expires: time }
    end
  end
end
