# frozen_string_literal: true

module Products
  # Service who return the products depend the data sended
  class List < BaseService
    def initialize(product_params)
      @order = product_params[:order]
      @filter_tag = product_params[:filter_tag]
      @attribute = product_params[:attribute]
      @page = product_params[:page]
      @size = product_params[:size]
    end

    def call
      products = validator
      products = sorting_out(products)
      products = filter_by_tag(products)
      @pagy, products = pagy_custom(products)
      product_with_metadata(products)
    end

    private

    def validator
      validate_paramas = Products::FilterParams.new(order: @order, filter_tag: @filter_tag, attribute: @attribute, page: @page,
                                                    size: @size)
      validate_paramas.validate!
    end

    def sorting_out(validate_params)
      Products::ProductQuery.new.order_by(validate_params.attribute, validate_params.order)
    end

    def filter_by_tag(products)
      Products::ProductQuery.new(products).filter_tag(@filter_tag)
    end

    def pagy_custom(collection)
      pagy = Pagy.new(count: collection.count(:all), page: @page, items: @size)
      [pagy, collection.offset(pagy.offset).limit(pagy.items)]
    end

    def product_with_metadata(products)
      json = {}
      json[:data] = { products: Products::ProductPresenter.for_collection.new(products) }
      json[:pagination] = { page: @pagy.page, size: @size, prev: @pagy.prev, next: @pagy.next }
      json
    end
  end
end
