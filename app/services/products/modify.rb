# frozen_string_literal: true

module Products
  class Modify < BaseService
    def initialize(product_params)
      @product_id = product_params[:product_id]
      @price = product_params[:price]
      @stock = product_params[:stock]
    end

    def call
      product = validator
      update_product(product)
    end

    private

    def validator
      validate_paramas = Products::UpdateForm.new(product_id: @product_id, price: @price, stock: @stock)
      validate_paramas.validate!
    end

    def update_product(product_validate)
      product = Product.find_by(id: product_validate.product_id)
      old_price = product.price
      if product.update(stock: product_validate.stock, price: product_validate.price)
        PriceChanges::Saver.call(product_id: product_validate.product_id, old_price: old_price,
                                 new_price: product_validate.price)
        'Product updated'
      else
        'Error: Product cant be updated'
      end
    end
  end
end
