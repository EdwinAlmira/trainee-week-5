# frozen_string_literal: true

module Products
  class Delete < BaseService
    def initialize(product_id)
      @product_id = product_id
    end

    def call
      product = validator
      delete_product(product)
    end

    private

    def validator
      validate_paramas = Products::DeleteForm.new(id: @product_id)
      validate_paramas.validate!
    end

    def delete_product(product_validate)
      product = Product.find_by(id: product_validate.id)
      if product.destroy
        'Product deleted'
      else
        'Error: The product its associated with some components'
      end
    end
  end
end
