# frozen_string_literal: true

module Products
  # Service who return the products depend the data sended
  class Add < BaseService
    def initialize(product_params)
      @name = product_params[:name]
      @price = product_params[:price]
      @stock = product_params[:stock]
      @description = product_params[:description]
      @picture = product_params[:picture]
    end

    def call
      product = validator
      save_product(product)
    end

    private

    def validator
      validate_paramas = Products::ProductAddForm.new(name: @name, price: @price, stock: @stock,
                                                      description: @description, picture: @picture)
      validate_paramas.validate!
    end

    def save_product(product_validate)
      product = Product.new(name: product_validate.name, price: product_validate.price, stock: product_validate.stock,
                            description: product_validate.description, picture: product_validate.picture)
      if product.save
        'Product added'
      else
        'Error to add product'
      end
    end
  end
end
