# frozen_string_literal: true

module Likes
  # Service for create a new like from the API
  class RemoveLike < LikeService
    def initialize(like_params)
      @product_id = like_params[:product_id]
      @user_id = like_params[:user_id]
    end

    def call
      like = validator
      dislike(like)
    end

    private

    def validator
      like = Likes::LikeForm.new(user_id: @user_id, product_id: @product_id)
      like.validate!
    end

    def dislike(valid_like)
      like = Like.where(user_id: valid_like.user_id, product_id: valid_like.product_id)[0]
      like.destroy
      Likes::SubsLikes.perform_now(valid_like.product_id)
      present_like(like, 'disliked')
    end
  end
end
