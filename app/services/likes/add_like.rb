# frozen_string_literal: true

module Likes
  # Service for create a new like from the API
  class AddLike < LikeService
    def initialize(like_params)
      @product_id = like_params[:product_id]
      @user_id = JsonWebToken.decode(like_params[:token])[:user_id]
    end

    def call
      like = validator
      save_like(like)
    end

    private

    def validator
      new_like = Likes::LikeForm.new(user_id: @user_id, product_id: @product_id)
      new_like.validate!
    end

    def save_like(validate_like)
      liked = already_liked(validate_like)
      if liked.nil?
        like = Like.new(user_id: validate_like.user_id, product_id: validate_like.product_id)
        saved?(like)
      else
        Likes::RemoveLike.call(product_id: @product_id, user_id: @user_id)
      end
    end

    def already_liked(validate_like)
      Like.find_by(user_id: validate_like.user_id, product_id: validate_like.product_id)
    end

    def saved?(like)
      if like.save
        Likes::SumLikes.perform_now(@product_id)
        present_like(like, 'liked')
      else
        raise CustomError.new('Liking error', 'The like cant be saved', 400)
      end
    end
  end
end
