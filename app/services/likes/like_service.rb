# frozen_string_literal: true

module Likes
  class LikeService < BaseService
    def present_like(like, message)
      nice_like = Likes::LikePresenter.new(like)
      json = {}
      json[:data] = { like: nice_like }
      json[:message] = message
      json
    end
  end
end
