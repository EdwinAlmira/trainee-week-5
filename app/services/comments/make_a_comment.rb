# frozen_string_literal: true

module Comments
  class MakeAComment < BaseService
    def initialize(comment_params)
      @type = comment_params[:type]
      @user_id = comment_params[:user_id]
      @description = comment_params[:description]
      @commented_id = comment_params[:id]
    end

    def call
      comment = validate_per_type
      save_comment(comment)
    end

    private

    def validator_purchase
      purchase = Comments::CommentPurchaseForm.new(purchase_id: @commented_id, description: @description,
                                                   user_id: @user_id)
      purchase.validate!
    end

    def validator_product
      product = Comments::CommentProductForm.new(product_id: @commented_id, description: @description,
                                                 user_id: @user_id)
      product.validate!
    end

    def validate_per_type
      if @type == 'purchase'
        new_purchase_comment(validator_purchase)
      else
        new_product_comment(validator_product)
      end
    end

    def new_purchase_comment(validate_commented)
      purchase = Purchase.find_by(id: validate_commented.purchase_id)
      Comment.new(commented: purchase, user_id: validate_commented.user_id,
                  description: validate_commented.description)
    end

    def new_product_comment(validate_commented)
      product = Product.find_by(id: validate_commented.product_id)
      Comment.new(commented: product, user_id: validate_commented.user_id,
                  description: validate_commented.description)
    end

    def save_comment(comment)
      if comment.save
        'Comment saved'
      else
        raise CustomError.new('Comment error', 'The comment cant be saved', 400)
      end
    end
  end
end
