# frozen_string_literal: true

module Purchases
  class ListView < BaseService
    def initialize(purchase_params)
      @user = purchase_params[:user]
      @purchase_id = purchase_params[:id]
    end

    def call
      define_list
    end

    private

    def define_list
      if @user.user_type_id == 1
        general_purchase
      else
        personal_purchase
      end
    end

    def general_purchase
      purchases = Purchases::PurchaseQuery.new.paid_purchases
      Purchases::PurchaseQuery.new(purchases).order_by_date
    end

    def personal_purchase
      purchases = Purchases::PurchaseQuery.new.paid_purchases
      purchases = Purchases::PurchaseQuery.new(purchases).by_user(@user)
      Purchases::PurchaseQuery.new(purchases).order_by_date
    end
  end
end
