# frozen_string_literal: true

module Purchases
  class Delete < BaseService
    def initialize(purchase_params)
      @purchase_id = purchase_params[:id]
    end

    def call
      purchase = validator
      delete_purchase(purchase)
    end

    private

    def validator
      purchase = Purchases::DeleteForm.new(id: @purchase_id)
      purchase.validate!
    end

    def delete_purchase(validated_params)
      purchase = Purchase.find_by(id: validated_params.id)
      if purchase.update(status: :cancel)
        'Deleted purchase'
      else
        raise CustomError.new('Purchase error', 'The purchase cant be destroy it', 400)
      end
    end
  end
end
