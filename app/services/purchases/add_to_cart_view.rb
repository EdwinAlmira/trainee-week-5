# frozen_string_literal: true

module Purchases
  class AddToCartView < BaseService
    def initialize(purchase_params)
      @user_id = purchase_params[:user_id]
      @quantity = purchase_params[:quantity]
      @product_id = purchase_params[:product_id]
      @cart_id = Carts::Assign.call(@user_id)
    end

    def call
      purchase = validator
      save_purchase(purchase)
    end

    private

    def validator
      purchase = Purchases::PurchaseForm.new(cart_id: @cart_id, product_id: @product_id, quantity: @quantity)
      purchase.validate!
    end

    def save_purchase(valid_purchase)
      purchase = Purchase.new(cart_id: valid_purchase.cart_id, product_id: valid_purchase.product_id,
                              quantity: valid_purchase.quantity, status: :pending)
      saved?(purchase)
    end

    def saved?(purchase)
      raise CustomError.new('Purchase error', 'The purchase cant be added', 400) unless purchase.save

      'Product added to cart'
    end
  end
end
