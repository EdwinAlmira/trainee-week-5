# frozen_string_literal: true

module Purchases
  class List < BaseService
    def initialize(token)
      @user_id = JsonWebToken.decode(token)[:user_id]
    end

    def call
      purchases = filtering
      purchases = sorting_out(purchases)
      purchase_with_metadata(purchases)
    end

    private

    def filtering
      all = PurchaseApiQuery.new.paid_purchases
      Purchases::PurchaseApiQuery.new(all).by_user(User.find_by(id: @user_id))
    end

    def sorting_out(purchases)
      Purchases::PurchaseApiQuery.new(purchases).order_by_date
    end

    def purchase_with_metadata(purchases)
      hash = {}
      hash[:data] = { purchases: Purchases::PurchasePresenter.for_collection.new(purchases) }
      hash
    end
  end
end
