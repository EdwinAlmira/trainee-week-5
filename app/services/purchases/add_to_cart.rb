# frozen_string_literal: true

module Purchases
  class AddToCart < BaseService
    def initialize(purchase_params)
      @user_id = JsonWebToken.decode(purchase_params[:token])[:user_id]
      @quantity = purchase_params[:quantity]
      @product_id = purchase_params[:product_id]
      @cart_id = Carts::Assign.call(@user_id)
    end

    def call
      purchase = validator
      purchase = save_purchase(purchase)
      purchase_with_metadata(purchase)
    end

    private

    def validator
      purchase = Purchases::PurchaseForm.new(cart_id: @cart_id, product_id: @product_id, quantity: @quantity)
      purchase.validate!
    end

    def save_purchase(valid_purchase)
      purchase = Purchase.new(cart_id: valid_purchase.cart_id, product_id: valid_purchase.product_id,
                              quantity: valid_purchase.quantity, status: :pending)
      saved?(purchase)
    end

    def saved?(purchase)
      if purchase.save
        purchase
      else
        raise CustomError.new('Purchase error', 'The purchase cant be saved', 400)
      end
    end

    def purchase_with_metadata(purchases)
      hash = {}
      hash[:data] = { purchases: Purchases::PurchasePresenter.new(purchases) }
      hash
    end
  end
end
