# frozen_string_literal: true

module PriceChanges
  class Saver < BaseService
    def initialize(price_changes_params)
      @old_price = price_changes_params[:old_price]
      @new_price = price_changes_params[:new_price]
      @product_id = price_changes_params[:product_id]
    end

    def call
      price_change = validator
      save_prices(price_change)
    end

    private

    def validator
      price_change = PriceChanges::PriceChangeForm.new(product_id: @product_id, old_price: @old_price,
                                                       new_price: @new_price)
      price_change.validate!
    end

    def save_prices(validated_params)
      if validated_params.old_price != validated_params.new_price
        PriceChange.create(product_id: validated_params.product_id, old_price: validated_params.old_price)
      end
    end
  end
end
