# frozen_string_literal: true

Rails.application.routes.draw do
  root 'inventory/products#index'
  devise_for :users

  namespace :inventory do
    resources :products
    resources :carts, only: %i[show update]
    resources :purchases, only: %i[index show new create destroy]
    resources :tags, only: %i[index new create destroy]
    resources :product_tags, only: %i[create]
    resources :users, only: %i[show]
    resources :user_reviews
  end

  namespace :store do
    resources :likes, only: %i[new create destroy]
    resources :comments, only: %i[new create]
  end

  namespace :api do
    namespace :v1 do
      resources :authentication, param: :_username
      post '/auth/login', to: 'authentication#login'
      namespace :customers do
        resources :likes, only: %i[create]
        resources :purchases, only: %i[index create]
        resources :products, only: %i[index]
        get '/*a', to: 'application#not_found'
      end
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
